// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ComponentDialog, TextPrompt, ChoicePrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { CartItemDialog, CART_ITEM_DIALOG } = require('./cartItemDialog');
const { Cart } = require('../cart');

const PURCHASE_DIALOG = 'PURCHASE_DIALOG';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

const TEXT_PROMPT = 'TEXT_PROMPT';
const COURIER_PROMPT = 'COURIER_PROMPT';

class PurchaseDialog extends ComponentDialog {
    constructor() {
        super(PURCHASE_DIALOG);

        this.addDialog(new TextPrompt(TEXT_PROMPT));
        this.addDialog(new ChoicePrompt(COURIER_PROMPT));
        
        this.addDialog(new CartItemDialog());
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.cartItemsStep.bind(this),
            this.senderNameStep.bind(this),
            this.receiverNameStep.bind(this),
            this.addressStep.bind(this),
            this.phoneStep.bind(this),
            this.courierStep.bind(this),
            this.summaryStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async cartItemsStep(stepContext) {
        stepContext.values.cart = new Cart();
        console.log('beginning', stepContext.values.cart);
        return await stepContext.beginDialog(CART_ITEM_DIALOG);
    }

    async senderNameStep(stepContext) {
        const cartItems = Array.isArray(stepContext.result) ? stepContext.result : [];
        stepContext.values.cart.items = cartItems;
        console.log('cart', stepContext.values.cart);

        // Should not be able to checkout 
        if (cartItems.length === 0) {
            return await stepContext.endDialog();
        }

        return await stepContext.prompt(TEXT_PROMPT, {
            prompt: `Boleh sebutkan nama pemesan?`
        });
    }

    async receiverNameStep(stepContext) {
        stepContext.values.cart.sender = stepContext.result;
        console.log('cart', stepContext.values.cart);
        
        return await stepContext.prompt(TEXT_PROMPT, {
            prompt: `Oke. Boleh sebutkan nama penerima?`
        });
    }

    async addressStep(stepContext) {
        stepContext.values.cart.receiver = stepContext.result;
        console.log('cart', stepContext.values.cart);

        return await stepContext.prompt(TEXT_PROMPT, {
            prompt: `Baik. Boleh minta alamat pengiriman? Lengkap beserta kode posnya ya.`
        });
    }

    async phoneStep(stepContext) {
        stepContext.values.cart.address = stepContext.result;
        console.log('cart', stepContext.values.cart);

        return await stepContext.prompt(TEXT_PROMPT, {
            prompt: `Oke. Boleh minta nomor telepon penerima?`
        });
    }

    async courierStep(stepContext) {
        stepContext.values.cart.phone = stepContext.result;
        console.log('cart', stepContext.values.cart);

        const availableCouriers = this.getCourierOptions();

        return await stepContext.prompt(TEXT_PROMPT, {
            choices: availableCouriers.map(el => el.value),
            prompt: `Oke, sudah saya catat. Mau dikirim menggunakan apa? 
Silahkan pilih angkanya saja:
${availableCouriers.map(el => `${el.value}. ${el.label}`).join(`\n`)}
`
        });
    }

    async summaryStep(stepContext) {
        const availableCouriers = this.getCourierOptions();
        stepContext.values.cart.courier = availableCouriers.find(el => el.value === `${stepContext.result}`);

        const cart = stepContext.values.cart;
        const subtotal = cart.items.reduce((accumulator, el) => accumulator + (el.qty * el.price), 0);
        const shippingCost = 25000;

        await stepContext.context.sendActivity(`Baik. Berikut adalah detil pesanan Anda:`);
        await stepContext.context.sendActivity(`${cart.items.map(el => `${el.qty}x ${el.name} @ Rp ${Intl.NumberFormat('id-ID').format(el.price)}\n\n... Rp ${Intl.NumberFormat('id-ID').format(el.qty * el.price)}\n\n`).join('')}

=== Subtotal Rp ${Intl.NumberFormat('id-ID').format(subtotal)}
`)

        await stepContext.context.sendActivity(`
Pengirim: ${cart.sender}\n\n
Penerima: ${cart.receiver}\n\n
${cart.address}

Pengiriman: ${cart.courier.label}
        `);

        await stepContext.context.sendActivity(`==========\n\n
Subtotal: Rp ${Intl.NumberFormat('id-ID').format(subtotal)}\n\n
Biaya Kirim: Rp ${Intl.NumberFormat('id-ID').format(shippingCost)}\n\n
----------
Total Rp ${Intl.NumberFormat('id').format(subtotal + shippingCost)}\n\n
==========
`);

        await stepContext.context.sendActivity(`Silahkan melakukan transfer ke rekening berikut:\
Bank BCA KCU Sunburst
0123456789
Atas Nama Seller, Inc.

Lakukan konfirmasi pembayaran segera agar kami dapat memproses pesanan Anda.
`);
        await stepContext.context.sendActivity(`Terima kasih telah berbelanja di Home Shopping.`);
        
        return await stepContext.endDialog();
    }

    getCourierOptions() {
        return [
            { value: '1', label: 'GoSend' },
            { value: '2', label: 'SiCepat' },
            { value: '3', label: 'Wahana' },
        ];
    }


}

module.exports.PurchaseDialog = PurchaseDialog;
module.exports.PURCHASE_DIALOG = PURCHASE_DIALOG;
