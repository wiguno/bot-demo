// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ComponentDialog, NumberPrompt, TextPrompt, ChoicePrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { CartItem } = require('../cartItem');

const CART_ITEM_DIALOG = 'CART_ITEM_DIALOG';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

const SKU_PROMPT = 'SKU_PROMPT';
const QTY_PROMPT = 'QTY_PROMPT';
const CANCEL_PURCHASE_PROMPT = 'CANCEL_PURCHASE_PROMPT';

const CART_ITEM_INPUT_STOP_CODE = 'N';

class CartItemDialog extends ComponentDialog {
    constructor() {
        super(CART_ITEM_DIALOG);

        this.addDialog(new TextPrompt(SKU_PROMPT, this.skuPromptValidator.bind(this)));
        this.addDialog(new NumberPrompt(QTY_PROMPT, this.qtyPromptValidator));
        this.addDialog(new ChoicePrompt(CANCEL_PURCHASE_PROMPT));

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.skuStep.bind(this),
            this.qtyStep.bind(this),
            this.loopStep.bind(this),
            this.cancelCartStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async skuStep(stepContext) {
        // Reset flag for input cancellation; used only when user deliberately wants to cancel the input.
        stepContext.values.flagCancelInput = false;

        // Repeat counter, so we can customize the message for each turn
        stepContext.values.counter = (stepContext.options.counter || 0) + 1;

        // Initialize new item selector
        stepContext.values.item = new CartItem();
        const cartItems = Array.isArray(stepContext.options.cartItems) ? stepContext.options.cartItems : [];
        stepContext.values.cartItems = cartItems;
        
        let message;
        if (stepContext.values.counter === 1) {
            message = `Barang apa yang ingin dibeli? Silahkan balas dengan kode barangnya.`;
        } else {
            message = `Apa ada barang lain yang ingin dibeli? Balas dengan kode barangnya.\n\nBalas dengan ${CART_ITEM_INPUT_STOP_CODE} bila telah selesai memilih barang.`
        }

        return await stepContext.prompt(SKU_PROMPT, {
            prompt: message
        });
    }

    async qtyStep(stepContext) {
        const sku = stepContext.result;

        // If user replies with CART_ITEM_INPUT_STOP_CODE
        if (sku.toUpperCase() === CART_ITEM_INPUT_STOP_CODE) {
            stepContext.values.flagCancelInput = true;
            return await stepContext.next();
        }
        
        const product = this.getProductBySku(sku);
        const productName = product.name;

        stepContext.values.item.sku = product.sku;
        stepContext.values.item.price = product.price;
        stepContext.values.item.name = productName;

        return await stepContext.prompt(QTY_PROMPT, {
            prompt: `Berapa jumlah ${productName} yang ingin dibeli? Balas dengan angka 0 bila tidak jadi membeli barang ini.`
        })
    }

    async loopStep(stepContext) {
        const qty = stepContext.result;
        let cartItems = stepContext.values.cartItems;
        
        if (stepContext.values.flagCancelInput) {
            // Offer to cancel cart if user deliberately cancels input and cart is empty
            if (cartItems.length === 0) {
                return await stepContext.prompt(CANCEL_PURCHASE_PROMPT, {
                    choices: [
                        { value: 'Y', synonim: 'y' },
                        { value: 'T', synonim: 't' },
                    ],
                    prompt: `Apakah Anda akan membatalkan pembelian?`
                });
            }

            // Return to parent flow if user deliberately wants to stop the input
            return await stepContext.endDialog(cartItems);
        }

        // If current qty is 0, means user decides not to purchase this item. 
        // Skip adding item to cart and continue asking for next item.
        if (qty > 0) {
            stepContext.values.item.qty = qty;

            // Update existing item, if it's already in cart, or add the item to cart
            const itemIdx = cartItems.findIndex(item => `${item.sku}`.toUpperCase() === `${stepContext.values.item.sku}`.toUpperCase());
            console.log('itemIdx', itemIdx);
            if (itemIdx > -1) {
                cartItems[itemIdx] = stepContext.values.item;
            } else {
                cartItems.push(stepContext.values.item);
            }
        }

        return await stepContext.replaceDialog(CART_ITEM_DIALOG, { cartItems, counter: stepContext.values.counter });
    }

    async cancelCartStep(stepContext) {
        const stopCartInput = stepContext.result.value;
        switch (stopCartInput.toUpperCase()) {
            case 'Y':
                await stepContext.context.sendActivity(`Oke. Pesanan Anda telah dibatalkan.`);
                return await stepContext.endDialog([]);
            case 'T':
                return await stepContext.replaceDialog(CART_ITEM_DIALOG);
        }
    }

    async skuPromptValidator(promptContext) {
        // If user replies with CART_ITEM_INPUT_STOP_CODE
        if (promptContext.recognized.succeeded && `${promptContext.recognized.value}`.toUpperCase() === CART_ITEM_INPUT_STOP_CODE) {
            return true;
        }

        const sku = promptContext.recognized.value;
        const isValidSku = (promptContext.recognized.succeeded && this.getProductBySku(sku) !== null);
        if (!isValidSku) {
            await promptContext.context.sendActivity(`SKU yang Anda berikan tidak terdaftar. Silahkan balas dengan SKU yang ingin Anda beli, atau balas dengan ${CART_ITEM_INPUT_STOP_CODE} bila telah selesai.`);
        }
        
        return isValidSku;
    }

    async qtyPromptValidator(promptContext) {
        const isValidQty = (promptContext.recognized.succeeded && promptContext.recognized.value >= 0);
        if (!isValidQty) {
            await promptContext.context.sendActivity(`Mohon maaf, mohon balas dengan jumlah yang ingin dibeli, atau balas dengan angka 0 bila tidak jadi membeli barang ini.`);
        }

        return isValidQty;
    }

    getProducts() {
        return [
            { sku: '001', name: 'Sabun', price: 13500 },
            { sku: '002', name: 'Kecap', price: 17000 },
            { sku: '003', name: 'Susu', price: 93000 },
            { sku: '004', name: 'Shampoo', price: 27500 },
            { sku: '005', name: 'Kentang', price: 13500 },
        ];
    }

    getProductBySku(sku) {
        return this.getProducts().find(el => `${el.sku}`.toUpperCase() === `${sku}`.toUpperCase()) || null;
    }
}

module.exports.CartItemDialog = CartItemDialog;
module.exports.CART_ITEM_DIALOG = CART_ITEM_DIALOG;
