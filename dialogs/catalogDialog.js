// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ComponentDialog, ChoicePrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { ActivityTypes } = require('botbuilder');
const { PurchaseDialog, PURCHASE_DIALOG } = require('./purchaseDialog');
const path = require('path');
const fs = require('fs');

const CATALOG_DIALOG = 'CATALOG_DIALOG';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
const CHOICE_PROMPT = 'CHOICE_PROMPT';

class CatalogDialog extends ComponentDialog {
    constructor() {
        super(CATALOG_DIALOG);

        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new PurchaseDialog());
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.menuSelector.bind(this),
            this.menuSelected.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async menuSelector(stepContext) {
        await stepContext.context.sendActivity({
            type: ActivityTypes.Message,
            text: 'Berikut katalog Home Delivery',
            attachments: [this.getInlineAttachment()]
        });

        return await stepContext.prompt(CHOICE_PROMPT, {
            choices: ['1', '2'],
            prompt: `Silahkan ketik angka pilihan sebagai balasan.
1. Lanjut ke pembelian produk
2. Terhubung dengan operator
            `
        });
    }

    async menuSelected(stepContext) {
        switch (stepContext.result.value) {
            case '1':
                return await stepContext.beginDialog(PURCHASE_DIALOG);
            case '2':
                return await stepContext.context.sendActivity('Not available yet');
        }
    }

    getInlineAttachment() {
        const imageData = fs.readFileSync(path.join(__dirname, '../resources/attachment.png'));
        const base64Image = Buffer.from(imageData).toString('base64');

        return {
            name: 'katalog.png',
            contentType: 'image/png',
            contentUrl: `data:image/png;base64,${base64Image}`
        };
    }
}

module.exports.CatalogDialog = CatalogDialog;
module.exports.CATALOG_DIALOG = CATALOG_DIALOG;
