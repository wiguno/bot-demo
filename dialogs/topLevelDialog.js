// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { ComponentDialog, ChoicePrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { CatalogDialog, CATALOG_DIALOG } = require('./catalogDialog');
const { PurchaseDialog, PURCHASE_DIALOG } = require('./purchaseDialog');

const TOP_LEVEL_DIALOG = 'TOP_LEVEL_DIALOG';
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';
const CHOICE_PROMPT = 'CHOICE_PROMPT';

class TopLevelDialog extends ComponentDialog {
    constructor() {
        super(TOP_LEVEL_DIALOG);

        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new PurchaseDialog(PURCHASE_DIALOG));
        this.addDialog(new CatalogDialog());
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.menuSelector.bind(this),
            this.menuSelected.bind(this),
            this.loopStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async menuSelector(stepContext) {
        // Repeat counter, so we can customize the message for each turn
        stepContext.values.counter = (stepContext.options.counter || 0) + 1;

        let message;
        if (stepContext.values.counter === 1) {
            message = `Selamat datang. Apa yang bisa kami bantu?`;
        } else {
            message = `Apa ada lagi yang bisa kami bantu?`;
        }

        message += `\n\nSilakan ketik angka pilihan sebagai balasan.
1. Katalog
2. Konfirmasi pembayaran
3. Cek pengiriman barang
4. Beli barang
5. Bantuan lainnya
        `;

        return await stepContext.prompt(CHOICE_PROMPT, {
            choices: ['1', '2', '3', '4', '5'],
            prompt: message
        });
    }

    async menuSelected(stepContext) {
        switch (stepContext.result.value) {
            case '1':
                return await stepContext.beginDialog(CATALOG_DIALOG);
            case '2': 
                await stepContext.context.sendActivity('Maaf, fitur ini belum tersedia.');
                return await stepContext.replaceDialog(TOP_LEVEL_DIALOG, { counter: stepContext.values.counter });
            case '3': 
                await stepContext.context.sendActivity('Maaf, fitur ini belum tersedia.');
                return await stepContext.replaceDialog(TOP_LEVEL_DIALOG, { counter: stepContext.values.counter });
            case '4': 
                return await stepContext.beginDialog(PURCHASE_DIALOG);
            case '5': 
                await stepContext.context.sendActivity('Maaf, fitur ini belum tersedia.');
                return await stepContext.replaceDialog(TOP_LEVEL_DIALOG, { counter: stepContext.values.counter });
        }
    }

    async loopStep(stepContext) {
        return await stepContext.replaceDialog(TOP_LEVEL_DIALOG, { counter: stepContext.values.counter });
    }
}

module.exports.TopLevelDialog = TopLevelDialog;
module.exports.TOP_LEVEL_DIALOG = TOP_LEVEL_DIALOG;
