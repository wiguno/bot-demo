// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

class CartItem {
    constructor() {
        this.sku = null;
        this.name = null;
        this.price = 0;
        this.qty = 0;
    }
}

module.exports.CartItem = CartItem;
