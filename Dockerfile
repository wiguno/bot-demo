FROM node:13.13.0-alpine

COPY . /bot-demo
EXPOSE 3978
CMD ["node /bot-demo/index.js"]