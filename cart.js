// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

class Cart {
    constructor() {
        this.sender = null;
        this.receiver = null;
        this.phone = null;
        this.address = null;
        this.courier = null;
        this.items = [];
    }
}

module.exports.Cart = Cart;
