# Bot Example

This is a simple commerce bot, built using Microsoft Bot Framework.

## Getting Started

First, you will have to sign up to Microsoft Azure Cloud, and register your Bot Service there.

1. Login to your Azure account
2. Select `Bot Services` 
3. Click `Add` button to add your bot
4. Select `Bot Channels Registration`
5. On the next page, click `Create`

Next, you will have to acquire your bot's Application ID and secret.

1. Browse to your bot page, and click the Resource group link.
2. Click `Deployments` on the left sidebar
3. Select your bot name under the `Deployment name`
4. Click `Inputs on the left sidebar
5. Copy the `appId` and `appSecret` strings
6. Create a `.env` file in the root folder of this project, and copy and paste the configuration as shown below

```
MicrosoftAppId={{appId}}
MicrosoftAppPassword={{appSecret}}
```

Replace {{appId}} and {{appSecret}} with the strings you got from the previous step. Do not forget to remove the handlebars.

7. Do not forget to do `npm install` to install the dependencies.

## How to run

To ease the development process, you will need to download Microsoft Bot Framework Emulator, which you can download from here: https://github.com/microsoft/BotFramework-Emulator

1. Run `npm run watch`. Take note of the URL and port used.
2. Open the Microsoft Bot Framework Emulator.
3. Click `Open Bot`
4. Fill in the Bot URL with the URL and port you have from the first step. Append `/api/messages` path to the URL.
5. Fill in also the Microsoft App ID and Microsoft App password (the secret string) you received from the previous step.
6. Click `Connect` to start communicating with your bot.

## How to Deploy

To deploy your bot, you will have to register you bot's endpoint to Azure Bot Services. 

1. Deploy your bot somehwere accessible by https protocol.
2. Go to `Bot Services` inside your Azure dashboard.
3. Click your bot name
4. Click `Settings` under `Bot management`
5. Fill the `Messaging endpoint` field with your bot's URL. Don't forget to add `/api/messages` if necessary.
6. Click `Save` button on the top of the page.

## How to Integrate to Chat Platforms

To integrate to Chat Platforms, we are going to utilize Azure Service.

1. Go to `Bot Services` inside your Azure dashboard.
2. Click your bot name
3. Click `Channels` under `Bot management`
4. Select the channel you want to integrate with, and follow the instructions.

## Notes

To ease the integration to other Chat Platforms, use ngrok to serve your bot. By doing this you can monitor every requests and responses your bot application receive and send. Use the https link provided by ngrok for the `Messaging endpoint` configuration in you Azure dashboard.

